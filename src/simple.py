#!/usr/bin/env python3
# encoding: utf-8

sicniki = ["s", "c", "z"]
sumniki = ["š", "č", "ž"]

def kandidati():
    kandidati = []
    with open("../data/sbsj.txt") as file:
        lines = [line.rstrip() for line in file]
    for word in lines:
        if len(word) < 2:
            continue
        kandidati.append(word)
    return kandidati


def vec_sumnikov(kandidati):
    """
    Besede, ki imajo več šumnikov (vse tri ali kombinacije po dva izmed njih Š-Č, Ž-Š, Č-Ž, vrsti red ni pomemben).
    Ne smejo imet sičnikov. Primer bi bil npr. Žrebiček, češnje, meščan.

    >>> vec_sumnikov(["žrebiček", "tralala", "suša"])
    ['žrebiček']

    >>> vec_sumnikov(["češnje", "tralala", "suša"])
    ['češnje']

    >>> vec_sumnikov(["meščan", "tralala", "suša"])
    ['meščan']
    """
    kombinacije = []
    for word in kandidati:
        sumnikov = 0
        if len(word) > 8:
            continue
        for letter in word:
            if letter in sicniki:
                break
            if letter in sumniki:
                sumnikov += 1
        if sumnikov >= 2:
            kombinacije.append(word)
    return kombinacije

# Main
sumljavke = vec_sumnikov(kandidati())

with open("../data/šumljavke.txt", "w") as pairs_file:
    for sumljavka in sumljavke:
        pairs_file.write(f"{sumljavka}\n")