#!/usr/bin/env python3
# encoding: utf-8

import os
import pandas as pd
import pickle as pkl
import re

soglasniki = ["b", "c", "č", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "š", "t", "v", "z", "ž"]


def minimalni_pari():
    """
    Minimalni pari č in š, igranje s pandasom.
    """
    df = pd.read_csv("../data/sbsj.txt", names=["beseda"])
    pairs = []
    for word1 in df["beseda"][32763:36705]:
        if word1[0] in ["č", "š"]:
            for word2 in df["beseda"][294930:301557]:
                if word2[0] in ["č", "š"]:
                    if len(word1) == len(word2) and word1 != word2:
                        ndiff = 0
                        for i, letter in enumerate(word1):
                            ndiff += word2[i] != letter
                        if ndiff == 1:
                            pairs.append((word1, word2))

    with open("../data/čš_pari.txt", "w") as pairs_file:
        for pair in pairs:
            pairs_file.write(f"{pair[0]} {pair[1]}\n")
        print("Zapisano.")
    return pairs


def soglasniski_sklopi():
    """
    Besede, ki se začnejo z dvemi ali več soglasniki.
    """
    df = pd.read_csv("../data/sbsj.txt", names=["beseda"])
    ssklopi = dict()
    for word in df["beseda"]:
        if len(word) > 2:
            for i, letter in enumerate(word):
                if i in [0, 1] and letter not in soglasniki:
                    break
                if letter not in soglasniki:
                    if i not in ssklopi.keys():
                        ssklopi[i] = []
                    ssklopi[i].append(word)
                    break
    for key, arr in ssklopi.items():
        with open(f"../data/sklop_{key}.txt", "w") as sklopi_file:
            for beseda in arr:
                sklopi_file.write(f"{beseda}\n")
    print("Zapisano.")
    return ssklopi


if __name__ == "__main__":
    # minimalni_pari()
    soglasniski_sklopi()
